<?php

Class MonteCarlo{

	public $bestScore = -99999;
	public $bestMove;
	public $target;
	public $startTime;
	public $maxTime;//s
	public $totalMoves;

	public function __construct($target = 'hello world', $maxTime = 0.5){
		$this->startTime = microtime(true);
		$this->target = $target;
		$this->maxTime = $maxTime;
	}

	public function _eval($move){
		$score = 0;
		foreach(str_split($move) as $k => $l)
			$l == $this->target[$k] ? $score += 50 : $score -= abs(ord($this->target[$k]) - ord($l));
		//add 50 if letter is right otherwise substract ascci diff
		return $score;
	}

	public function randMove(){
		$length = strlen($this->target);
		// for($i=0, $string='', $chars = array_merge(range('a', 'z'), [' ']); $i<$length; ++$i)
		// 	$string .= $chars[rand(0, count($chars) - 1)];

		$chars = array_merge(range('a', 'z'), [' ']);
		$string = substr(str_shuffle(join($chars)), 0, $length);//faster than the above ?
		return $string;
	}

	public function run(){
		$moves = 0;
		while((microtime(true) - $this->startTime) < $this->maxTime){//while we have time
			$tmpMove = $this->randMove();
			$tmpScore = $this->_eval($tmpMove);
			if($tmpScore > $this->bestScore){//if temp move score > current best score
				$this->bestScore = $tmpScore;//update both
				$this->bestMove = $tmpMove;
			}
			$moves++;
		}
		$this->totalMoves = $moves;
		return $this->bestMove;//return best move
	}
}

$m = new MonteCarlo('jerome', 0.5);
$m->run();
echo "Best move found in $m->maxTime second is $m->bestMove after $m->totalMoves iterations\n";
echo "Execution time : " . (microtime(true) - $m->startTime) . "\n";